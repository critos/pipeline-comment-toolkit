# Pipeline Comment Toolkit

This repository provides a set of reusable components for enhancing your GitLab CI/CD pipelines with automated commenting functionality. These components are designed to streamline collaboration and communication within your development workflows by enabling automated comments on merge requests and issues.

## Repository Contents

1. **create_merge_request_comment.yaml**: This action allows you to automatically create comments on merge requests based on predefined triggers or conditions within your GitLab CI/CD pipelines. Use this action to provide feedback, status updates, or relevant information directly on merge requests, facilitating smoother code reviews and collaboration.

2. **post_merge_comment.yaml**: Post-merge comment action enables you to automatically add comments to merge requests after they've been successfully merged into the target branch. Use this action to notify stakeholders, celebrate achievements, or provide post-merge instructions seamlessly within your GitLab workflows.

3. **issue_comment.yaml**: This action enables automated commenting on GitLab issues, allowing you to keep stakeholders informed, address queries, or provide updates within the context of specific issues. Enhance visibility and communication within your projects by integrating automated comments directly into your issue management process.

## Use as a CI/CD component

To integrate these into your GitLab CI/CD pipelines, follow these steps:

```
include:
  # 1: include the component
  - component: gitlab.com/raibove/pipeline-comment-toolkit/pipeline-merge-comment@version
    # 2: set/override inputs
    inputs:
      message: "This is comment to repository"

```

## Contribution

Contributions, bug reports, and feature requests are welcome! If you encounter any issues or have suggestions for enhancing the functionality of these actions, feel free to submit a pull request or open an issue in the repository.

## License

This repository is licensed under the [MIT License](LICENSE).
