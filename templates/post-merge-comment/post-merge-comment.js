const axios = require('axios');

// Constants
const gitlabApiUrl = process.argv[2];
const accessToken = process.env.ACCESS_TOKEN;
const commentText = process.argv[3];
const commitMessage = process.env.CI_COMMIT_TITLE;
const projectId = process.env.CI_PROJECT_ID;

// Function to fetch merge request ID based on commit SHA
async function getMergeRequestID(branchName, projectId) {
    try {
        const url = `${gitlabApiUrl}/projects/${projectId}/merge_requests?source_branch=${branchName}&scope=all&state=merged`;
        const response = await axios.get(url, { headers: { 'Private-Token': accessToken } });

        if (response.data.length > 0) {
            return response.data[0].iid; // Return the merge request ID
        }
    } catch (error) {
        console.error('Error fetching merge request ID:', error.message);
    }
    
    return null; // Return null if merge request ID is not found
}

// Function to add comment to the merge request
async function addCommentToMergeRequest(mergeRequestId) {
    try {
        const response = await axios.post(`${gitlabApiUrl}/projects/${projectId}/merge_requests/${mergeRequestId}/notes`, {
            body: commentText,
        }, {
            headers: {
                'Private-Token': accessToken,
            },
        });

        console.log('Comment added successfully:', response.data);
    } catch (error) {
        console.error('Error adding comment:', error.response.data);
    }
}

// Main function to handle the workflow
async function main() {
    // Parse commit message to extract branch name
    const branchRegex = /Merge branch '(.+?)' into '(.+?)'/;
    const match = commitMessage.match(branchRegex);

    if (match && match.length === 3) {
        const branchName = match[1];
        try {
            const mergeRequestId = await getMergeRequestID(branchName, projectId);
            if (mergeRequestId) {
                console.log(`Merge Request ID for branch ${mergeRequestId}`);
                await addCommentToMergeRequest(mergeRequestId);
            } else {
                console.log(`Merge Request ID not found for branch '${branchName}'`);
            }
        } catch (error) {
            console.error('Error:', error.message);
        }
    } else {
        console.error('Error: cannot find branch name');
    }
}

main();
